import React from "react";
import { ExchangeRates } from "./components/ExchangeRates";

const App = () => {
  return (
    <div>
      My first apollo App!
      <ExchangeRates />
    </div>
  );
};

export default App;
